//
// Created by Justyna Kryś on 24/12/2019.
//
#ifndef ZAL_CPP_PIONKI_HH
#define ZAL_CPP_PIONKI_HH

#include <vector>
#include <string>
#include <iostream>


using namespace std;

class Pionek{
public:
    Pionek( char nr,int szyb):szybkosc(szyb),numer(nr){}
    virtual ~Pionek()=default;
    virtual std::string nazwa()=0;
    virtual std::string klasa()=0;

    bool rusz_sie();
    int szybk(){return szybkosc;}
    int x(){return x_;};
    int y(){return y_;};
    void set_x(int nx){ x_ = nx;};
    void set_y(int ny){ y_ = ny;};
  //  bool sprawdz(std::tuple<int,int> wektor);


protected:
    int szybkosc = 0;
    //std::vector<char> dostepny_teren; //???
    char numer; //potrzebne do numeracji pionkow ktorych jest wiecej niz 1
    int x_ = -1;
    int y_ = -1;
    int zasieg_ = 0;
    int ile_czekam_ = 0;

};

//Rebelianci
class Rebeliant:public Pionek{
public:
    Rebeliant(char numer,int szyb);
    ~Rebeliant()= default;
    void postaw_detektor();
    std::string nazwa(){return znak;};
    std::string klasa() {return klasa_;};
private:
    std::string znak="R";
    std::string klasa_ = "rebeliant";
};

class Programista:public Pionek{
public:
    Programista(char numer,int szyb);
    ~Programista()= default;
    std::string nazwa(){return znak;};
    std::string klasa() {return klasa_;};

private:
    std::string znak="P";
    std::string klasa_ = "programista";

};
//singleton
class LukeSkywalker:public Rebeliant{
public:
    LukeSkywalker(int szyb);
    ~LukeSkywalker()= default;
    std::string nazwa(){return znak;};
    std::string klasa() {return klasa_;};

private:
    std::string znak="LS";
    std::string klasa_ = "luke";
};
//singleton
class Ksiezniczka:public Rebeliant{
public:
    Ksiezniczka(int szyb);
    ~Ksiezniczka()= default;
    std::string nazwa(){return znak;};
    std::string klasa() {return klasa_;};

private:
    std::string znak="KL";
    std::string klasa_ = "ksiezniczka";
};

class Detector{
public:
    Detector();
    ~Detector();
    std::string klasa() {return klasa_;};

private:
    const static int zasieg;
    std::string klasa_ = "detektor";


};

//Imperium
class Robot:public Pionek{
public:
    Robot(char numer,int szyb);
    ~Robot()= default;
    std::string nazwa();
    std::string klasa() {return klasa_;};

private:
    std::string znak="r";
    std::string klasa_ = "robot";
};
//singleton
class LordVader:public Pionek{
public:
    LordVader(int szyb);
    ~LordVader()= default;
    std::string nazwa(){return znak;};
    std::string klasa() {return klasa_;};

private:
    std::string znak="LV";
    std::string klasa_ = "vader";
};
//singleton
class Kanclerz:public Pionek{
public:
    Kanclerz();
    ~Kanclerz()= default;
    std::string nazwa(){return znak;};
    std::string klasa() {return klasa_;};

private:
    std::string znak="KN";
    std::string klasa_ = "kanclerz";
};
#endif //ZAL_CPP_GRACZ_H



