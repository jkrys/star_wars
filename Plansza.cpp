//
// Created by Justyna Kryś on 24/12/2019.
//

#include "Plansza.hh"
#include "Pionki.cpp"
#include "Pole.cpp"

bool Plansza::ustaw_pionek(int x, int y,shared_ptr<Pionek> p){
    p->set_x(x);
    p->set_y(y);
    aktualna[x][y].pionek = p;
    cout<<" no stawiam "<<x<<" "<<y<<" ";
    return true;
}


void Plansza::zdejmij_pionek(int x, int y) {
    aktualna[x][y].pionek= nullptr;
}

Plansza::Plansza(int width,int height){
    for(int x=0;x<height;++x) {
        std::vector<Pole> wiersz;
        for (int y = 0; y < width; ++y)
            wiersz.emplace_back(Pole(1));
        aktualna.push_back(wiersz);
    }
}

Plansza::Plansza(const string& nazwa_pliku){
    string linia;
    fstream f;
    f.open(nazwa_pliku);

    if(f.good()){
        getline(f, linia);
        std::vector<Pole> wiersz;
        while(!f.eof()) {
            //cout << "plansza";
            getline(f, linia);
            for (int i=0;i<linia.size();++i) {
                cout << linia[i] <<" i "<< linia[i]- '0' << endl;
                wiersz.emplace_back(Pole(linia[i]- '0'));
            }
            aktualna.push_back(wiersz);
            wiersz.clear();
        }
    }

    f.close();

}


void Plansza::wczytaj_plansze(fstream f) {
    string linia;

    if(f.good())
    {
        std::vector<Pole> wiersz;
        getline(f, linia);

        while(!f.eof()) {
            getline(f, linia);
            for (auto a:linia)
                wiersz.emplace_back(Pole((int) a));
            aktualna.push_back(wiersz);
        }
    }
}


void Plansza::drukuj() {
    for (auto wiersz:aktualna) {
        for (auto p:wiersz)
            p.drukuj();
        std::cout << endl;
    }
}

