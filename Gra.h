//
// Created by Justyna Kryś on 26/12/2019.
//
#include <type_traits>
#ifndef ZAL_CPP_GRA_HH
#define ZAL_CPP_GRA_HH

#include "Plansza.hh"
#include "Fabryka.cpp"
#include "Gracz.h"

class Gra{
public:
    Gra(){}
    ~Gra(){}
    void graj();
    void dodaj_graczy();
    void dodaj_plansze();
    bool czy_koniec(){return false;}
    void kto_wygral(){cout<< "kto wygral"<<endl; }
    void wczytaj(string nazwa_pliku);
    shared_ptr<Plansza> wez_plansze();
private:
    shared_ptr<Fabryka> fabryka=std::make_shared<Fabryka>();
    shared_ptr<Plansza> plansza=std::make_shared<Plansza>();
    shared_ptr<Gracz> komputer=std::make_shared<Komputer>();
    shared_ptr<Gracz> czlowiek = std::make_shared<Czlowiek>();
    int runda=0;
    int czas_gry=0;
};

#endif //ZAL_CPP_GRA_H
