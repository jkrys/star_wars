//
// Created by Justyna Kryś on 26/12/2019.
//
#include <type_traits>
#ifndef ZAL_CPP_GRACZ_HH
#define ZAL_CPP_GRACZ_HH


#include "Plansza.hh"
#include "Pionki.hh"
#include <random>
using  namespace std;

class Gracz{
public:
    Gracz(){
        for (int i = 1; i >= -1; --i)
            for (int j = -1; j <= 1; ++j) {
                std::tuple<int, int> tup{i, j};
                to_check.push_back(tup);
            }
    }
    virtual ~Gracz()= default;
    virtual void rusz_sie()=0;
    virtual void stworz(std::stringstream &f,shared_ptr<Fabryka> fabryka,shared_ptr<Plansza> pl);
    string kim_jestes(){return name;}

//protected:
    bool czy_moj_pionek(shared_ptr<Pionek> p){
        for(auto i=pionki.cbegin();i!=pionki.cend();++i){
            if ((*i)==p){
                cout<<"MOJ";
                return true;}
        }
        cout<<"JEGO";
        return false;
    }
    std::string name;
    std::vector<std::tuple<int,int>> to_check;
    std::vector<shared_ptr<Pionek>> pionki;
    void dodaj_pionek(string f);
    shared_ptr<Plansza> plansza;

};

class Komputer:public Gracz{
public:
    Komputer() ;
    ~Komputer(){}
    void rusz_sie();
    bool sprawdz_pole(shared_ptr<Pionek> p, std::tuple<int,int> wektor){
        return pionek_pole[std::make_tuple(p->klasa(),(*plansza)[p->x()+std::get<0>(wektor)][p->y()+std::get<1>(wektor)].teren())];
    }

    shared_ptr<Pionek> daj_drugi_pionek(shared_ptr<Pionek> p, std::tuple<int,int> wektor){
        return (*plansza)[p->x()+std::get<0>(wektor)][p->y()+std::get<1>(wektor)].pionek;
    }
//jesli nullptr to zdejmij i postaw mnie
//jesli moj to jesli ja to mnie zdejmij else return flase
//jesli ktos to pojmij (zdejmij jego postaw mnie)
    bool sprawdz(shared_ptr<Pionek> p1 ,shared_ptr<Pionek> p2,std::tuple<int, int> wektor);
    std::string name="komputer";
private:
   // std::vector<std::tuple<int,int>> to_check;
    std::map<std::tuple<std::string,int>,bool> pionek_pole;
    std::map<std::tuple<std::string,std::string>,bool> pionek_pionek;

};

class Czlowiek:public Gracz{
public:
    Czlowiek();
    ~Czlowiek(){}
    void rusz_sie();
    //void stworz(string f){}
    std::string name="czlowiek";
    bool sprawdz_pole(shared_ptr<Pionek> p, std::tuple<int,int> wektor){
        return pionek_pole[std::make_tuple(p->klasa(),(*plansza)[p->x()+std::get<0>(wektor)][p->y()+std::get<1>(wektor)].teren())];
    }

    shared_ptr<Pionek> daj_drugi_pionek(shared_ptr<Pionek> p, std::tuple<int,int> wektor){
        return (*plansza)[p->x()+std::get<0>(wektor)][p->y()+std::get<1>(wektor)].pionek;
    }

    bool sprawdz(shared_ptr<Pionek> p1 ,shared_ptr<Pionek> p2,std::tuple<int, int> wektor);
private:
    std::map<std::tuple<std::string,int>,bool> pionek_pole;
    std::map<std::tuple<std::string,std::string>,bool> pionek_pionek;
    std::map<std::string,std::tuple<int,int>> komendy;

};


#endif //ZAL_CPP_GRACZ_H
