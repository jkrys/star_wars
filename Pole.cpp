//
// Created by Justyna Kryś on 25/01/2020.
//
#include "Pole.h"
#include "czytaj.cpp"
#include <iterator>

void Pole::drukuj(){
    //std::cout<<teren;
    std::string a(2,'0');
    //a[0]=a[1]='0';
    if (ma_detektor) a[0]=a[1]='D';
    if (pionek!=nullptr) a=pionek->nazwa();
    if (teren()==0) std::cout<<YELLOW<<a<<" "; //plaski
    else if (teren()==1) std::cout<<GREEN<<a<<" ";  //las
    else if (teren()==2) std::cout<<BLACK<<a<<" ";  // skaly
    else if (teren()==3) std::cout<<CYAN<<a<<" ";  // rzeka
    else if (teren()==4) std::cout<<MAGENTA<<a<<RESET<<" "; //ocean
    //drukuj_legende();

}

Pole::Pole(int w) {
    cout<<"A"<<w<<endl;
    teren_=fabryka->tworz(w);
    cout<<"TEREN"<<teren_<<endl;
    ma_detektor=false;
    pionek= nullptr;
}
