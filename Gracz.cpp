//
// Created by Justyna Kryś on 24/12/2019.
//

#include "Gracz.h"
#include "Plansza.cpp"

#include <iostream>
#include <fstream>
#include <sstream>
#include <random>

Czlowiek::Czlowiek():Gracz(){
    komendy["LD"]=std::make_tuple(1,-1);
    komendy["LG"]=std::make_tuple(-1,-1);
    komendy["PD"]=std::make_tuple(1,1);
    komendy["PG"]=std::make_tuple(-1,1);
    komendy["L"]=std::make_tuple(0,-1);
    komendy["D"]=std::make_tuple(1,0);
    komendy["G"]=std::make_tuple(-1,0);
    komendy["P"]=std::make_tuple(0,1);

    for (std::string n:{"programista","rebeliant", "luke","ksiezniczka"}) {
        for (int m:{0, 1, 2, 3, 4}){
            pionek_pole[std::make_tuple(n,m)]=false;
        }
    }

    for (std::string n:{"rebeliant", "luke","ksiezniczka"}) {
        for (int m:{0, 1, 2, 3}){
            pionek_pole[std::make_tuple(n,m)]=true;
        }
    }

    pionek_pole[std::make_tuple("programista",0)]=true;

    for (std::string n:{"programista","rebeliant", "luke","ksiezniczka"}) {
        for (std::string m:{"programista","rebeliant", "luke","ksiezniczka"}){
            pionek_pionek[std::make_tuple(n,m)]=false;
        }
    }

    for (std::string n:{"rebeliant", "luke","ksiezniczka"}) {
        for (std::string m:{"robot","vader","kanclerz"}){
            pionek_pionek[std::make_tuple(n,m)]=false;
        }
    }

    pionek_pionek[std::make_tuple("programista","robot")]=true;
    pionek_pionek[std::make_tuple("luke","vader")]=true;
    pionek_pionek[std::make_tuple("ksiezniczka","kanclerz")]=true;
}


Komputer::Komputer():Gracz() {

    bool a=false;
    for (std::string n:{"robot", "vader"}) {
        for (int m:{0, 1, 2, 3, 4}){
            pionek_pole[std::make_tuple(n,m)]=a;
        }
        a=not a;
    }
    pionek_pole[std::make_tuple("robot",0)]=true;
    pionek_pole[std::make_tuple("vader",4)]=false;

    for (std::string n:{"robot", "vader"}) {
        for (std::string m:{"rebeliant","ksieczniczka","luke"}){
            pionek_pionek[std::make_tuple(n,m)]=true;
        }
    }


    for (std::string n:{"robot", "vader"}) {
        for (std::string m:{"robot","vader","programista","kanclerz"}){
            pionek_pionek[std::make_tuple(n,m)]=false;
        }
    }
    pionek_pionek[std::make_tuple("vader","programista")]=true;


}

void Gracz::stworz(std::stringstream &f,shared_ptr<Fabryka> fabryka,shared_ptr<Plansza> pl){
    plansza = pl;

    std::string linia;
    int ilosc = 0;
    int pred = 0;
    int x=0;
    int y=0;
    std::string rodzaj;
    while (getline(f, linia)) {
        cout << "linia" << linia << endl;
        rodzaj.clear();

        for (int j = 0; j < linia.size(); ++j) {
            if (linia[j] != ' ') {
                rodzaj.push_back(linia[j]);
            } else {
                //cout << "ilosc " << linia[j + 1] << "pred" << linia[j + 3] << endl;
                pred = atoi(&linia[j + 1]);
                x = atoi(&linia[j + 3]);
                y = atoi(&linia[j + 5]);
                cout << "ilosc " << ilosc << "pred" << pred << endl;
                shared_ptr<Pionek> p=fabryka->stworz(rodzaj, pred);
                pionki.push_back(p);
                if (plansza->ustaw_pionek(x,y,p)) break;
                else cout<<"Blad ustawienia"<<endl;
                break;
            }
        }
    }
}

//void Gracz::dodaj_pionek(string f){}

void Komputer::rusz_sie(){
    cout<<"ruch kompa"<<endl;

    for (shared_ptr<Pionek> pionek:pionki) {
        std::vector<bool> ruchy;

        cout<<pionek->nazwa()<<endl;

        for (auto vec:to_check){
            ruchy.push_back(sprawdz_pole(pionek,vec));
        }
        for (int i=0;i<ruchy.size(); ++i){
            if (ruchy[i]) {
                cout << ruchy[i];
                if (sprawdz(pionek, daj_drugi_pionek(pionek, to_check[i]), to_check[i]))
                    break;
            }
        }
        cout<<endl;

    }
}
//jesli nullptr to zdejmij i postaw mnie
//jesli moj to jesli ja to mnie zdejmij else return flase
//jesli ktos to pojmij (zdejmij jego postaw mnie)
bool Czlowiek::sprawdz(shared_ptr<Pionek> p1 ,shared_ptr<Pionek> p2,std::tuple<int, int> wektor){
    if (p2) {
        if (p1->klasa() == "programista" && p2->klasa() == "robot") {
            plansza->zdejmij_pionek(p2->x(), p2->y());
            plansza->zdejmij_pionek(p1->x(), p1->y());
            plansza->ustaw_pionek(p1->x() + std::get<0>(wektor), p1->y() + std::get<1>(wektor), p1);
        } else if (czy_moj_pionek(p2)) return false;
        else if (p1->klasa() == "ksiezczniczka" && p2->klasa() == "kanclerz"){
            plansza->zdejmij_pionek(p1->x(), p1->y());
            plansza->zdejmij_pionek(p2->x(), p2->y());
            plansza->ustaw_pionek(p1->x() + std::get<0>(wektor), p1->y() + std::get<1>(wektor), p1);
        }
        else if (p1->klasa() == "luke" && p2->klasa() == "vader"){
            plansza->zdejmij_pionek(p1->x(), p1->y());
            plansza->zdejmij_pionek(p2->x(), p2->y());
            plansza->ustaw_pionek(p1->x() + std::get<0>(wektor), p1->y() + std::get<1>(wektor), p1);
        }
        else{
            plansza->zdejmij_pionek(p1->x(), p1->y());
        }
    }
    else {
        plansza->zdejmij_pionek(p1->x(), p1->y());
        plansza->ustaw_pionek(p1->x() + std::get<0>(wektor), p1->y() + std::get<1>(wektor), p1);
    }
    cout<<"ruszam "<<p1->nazwa()<<endl;
    return true;
}

void Czlowiek::rusz_sie() {
    for (shared_ptr<Pionek> pionek:pionki) {
        cout<<pionek->nazwa()<<endl;
        std::string komenda;
        cin>>komenda;

        std::string kierunek;

        if (komenda.size()==3){
            kierunek=komenda.substr(0,2);
        } else if (komenda.size()==2) kierunek=komenda.substr(0,1);

        std::tuple<int,int> vec=komendy[kierunek];
        cout<<std::get<0>(vec)<<" "<<std::get<1>(vec);
        if (sprawdz_pole(pionek,vec))
                if (sprawdz(pionek, daj_drugi_pionek(pionek, vec), vec)){
                    cout<<"Zrobione "<<komenda<<endl;
                }
    }
}

bool Komputer::sprawdz(shared_ptr<Pionek> p1 ,shared_ptr<Pionek> p2,std::tuple<int, int> wektor){
    if (p2) {
        if (p1->klasa() == "robot" && p2->klasa() == "programista") {
            plansza->zdejmij_pionek(p1->x(), p1->y());
        } else if (czy_moj_pionek(p2)) return false;
        else {
            plansza->zdejmij_pionek(p1->x(), p1->y());
            plansza->zdejmij_pionek(p2->x(), p2->y());
            plansza->ustaw_pionek(p1->x() + std::get<0>(wektor), p1->y() + std::get<1>(wektor), p1);
        }
    }
    else {
        plansza->zdejmij_pionek(p1->x(), p1->y());
        plansza->ustaw_pionek(p1->x() + std::get<0>(wektor), p1->y() + std::get<1>(wektor), p1);
    }
    cout<<"ruszam "<<p1->nazwa()<<endl;
    return true;
}
