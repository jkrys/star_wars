//
// Created by Justyna Kryś on 25/01/2020.
//

#ifndef ZAL_CPP_POLE_H
#define ZAL_CPP_POLE_H

#include "Pionki.hh"
#include <map>

using namespace std;


class Teren{
public:
    Teren(){}
    virtual ~Teren()= default;
    virtual int typ()=0;
    virtual shared_ptr<std::vector<std::string>> mozliwe_pionki()=0;
private:
    std::vector<std::string> mozliwe_pionki_={};
};

class Plaski: public Teren{
public:
    Plaski(){}
    ~Plaski()= default;
    int typ(){return typ_;}
    shared_ptr<std::vector<std::string>> mozliwe_pionki(){return make_shared<std::vector<std::string>>(mozliwe_pionki_);};
private:
    int typ_=0;
    std::vector<std::string> mozliwe_pionki_ = {"LV","KL","KN","LS","r","R","P"};


};
class Las: public Teren{
public:
    Las(){}
    ~Las()= default;
    int typ(){return typ_;}
    shared_ptr<std::vector<std::string>> mozliwe_pionki(){return make_shared<std::vector<std::string>>(mozliwe_pionki_);};
private:
    int typ_=1;
    std::vector<std::string> mozliwe_pionki_ = {"LV","KL","KN","LS","R"};

};
class Skaly: public Teren{
public:
    Skaly(){}
    ~Skaly()= default;
    int typ(){return typ_;}
    shared_ptr<std::vector<std::string>> mozliwe_pionki(){return make_shared<std::vector<std::string>>(mozliwe_pionki_);};
private:
    int typ_=2;
    std::vector<std::string> mozliwe_pionki_ = {"LV","KL","KN","LS","R"};

};
class Rzeka: public Teren{
public:
    Rzeka(){}
    ~Rzeka()=default;
    int typ(){return typ_;}
    shared_ptr<std::vector<std::string>> mozliwe_pionki(){return make_shared<std::vector<std::string>>(mozliwe_pionki_);};
private:
    int typ_=3;
    std::vector<std::string> mozliwe_pionki_ = {"LV","KL","KN","LS","R"};
};

class Ocean: public Teren{
public:
    Ocean(){}
    ~Ocean()= default;
    int typ(){return typ_;}
    shared_ptr<std::vector<std::string>> mozliwe_pionki(){return make_shared<std::vector<std::string>>(mozliwe_pionki_);};
private:
    int typ_=4;
    std::vector<std::string> mozliwe_pionki_ = {};

};

class Tworz{
public:
    virtual shared_ptr<Teren> operator()()=0;
};

class TworzPlaski :public Tworz{
public:
    shared_ptr<Teren> operator()(){
        return std::make_shared<Plaski>();
    }
};


class TworzSkaly :public Tworz{
public:
    shared_ptr<Teren> operator()(){
        return std::make_shared<Skaly>();
    }
};


class TworzRzeka :public Tworz{
public:
    shared_ptr<Teren> operator()(){
        return std::make_shared<Rzeka>();
    }
};
class TworzLas :public Tworz{
public:
    shared_ptr<Teren> operator()(){
        return std::make_shared<Las>();
    }
};

class TworzOcean :public Tworz{
public:
    shared_ptr<Teren> operator()(){
        return std::make_shared<Ocean>();
    }
};

class FabrykaTerenu{
public:
    FabrykaTerenu(){
        dyspozycja[0]=std::make_shared<TworzPlaski>();
        dyspozycja[1]=std::make_shared<TworzLas>();
        dyspozycja[2]=std::make_shared<TworzSkaly>();
        dyspozycja[3]=std::make_shared<TworzRzeka>();
        dyspozycja[4]=std::make_shared<TworzOcean>();

    }
    ~FabrykaTerenu()= default;
    shared_ptr<Teren> tworz(int i){

        return (*dyspozycja[i])();
    }
private:
    std::map<int,shared_ptr<Tworz>> dyspozycja;
};

class Pole{
public:
    Pole(int w);
    ~Pole(){}
    void drukuj();
    std::shared_ptr<Pionek> pionek;
    int teren(){return teren_->typ();}
    shared_ptr<FabrykaTerenu> fabryka=make_shared<FabrykaTerenu>();
    //bool postaw_pionek(shared_ptr<Pionek> p);
private:
    //std::shared_ptr<Pionek> pionek_;
    shared_ptr<Teren> teren_;
    bool ma_detektor;
};

#endif //ZAL_CPP_POLE_H
