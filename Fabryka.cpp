//
// Created by Justyna Kryś on 26/01/2020.
//
#include <map>

using namespace std;

class Produkcja{
public:
    virtual shared_ptr<Pionek> operator()(int szybkosc)=0;
    char liczba_robotow = '0';
    char liczba_rebeliantow = '0';
    char liczba_programistow = '0';
};
class ProdukcjaRobotow :public Produkcja{
public:
    shared_ptr<Pionek> operator()(int szybkosc){
        liczba_robotow+=1;
        return std::make_shared<Robot>(liczba_robotow,szybkosc);
    }
};
class ProdukcjaRebeliantow :public Produkcja{
public:
    shared_ptr<Pionek> operator()(int szybkosc){
        liczba_rebeliantow+=1;
        return std::make_shared<Rebeliant>(liczba_rebeliantow,szybkosc);
    }
};
class ProdukcjaProgramistow :public Produkcja{
public:
    shared_ptr<Pionek> operator()(int szybkosc){
        liczba_programistow+=1;
        return std::make_shared<Programista>(liczba_programistow,szybkosc);
    }
};
class ProdukcjaKsieczniczek :public Produkcja{
public:
    shared_ptr<Pionek> operator()(int szybkosc){
        return std::make_shared<Ksiezniczka>(szybkosc);
    }
};
class ProdukcjaLuke :public Produkcja{
public:
    shared_ptr<Pionek> operator()(int szybkosc){
        return std::make_shared<LukeSkywalker>(szybkosc);
    }
};
class ProdukcjaLord :public Produkcja{
public:
    shared_ptr<Pionek> operator()(int szybkosc){
        return std::make_shared<LordVader>(szybkosc);
    }
};
class ProdukcjaKanclerz :public Produkcja{
public:
    shared_ptr<Pionek> operator()(int szybkosc){
        return std::make_shared<Kanclerz>();
    }
};
class Fabryka{
public:
    Fabryka(){
        mapa_produkcji["rebeliant"]=make_shared<ProdukcjaRebeliantow>();
        mapa_produkcji["robot"]=make_shared<ProdukcjaRobotow>();
        mapa_produkcji["programista"]=make_shared<ProdukcjaProgramistow>();
        mapa_produkcji["ksiezniczka"]=make_shared<ProdukcjaKsieczniczek>();
        mapa_produkcji["luke"]=make_shared<ProdukcjaLuke>();
        mapa_produkcji["lord"]=make_shared<ProdukcjaLord>();
        mapa_produkcji["kanclerz"]=make_shared<ProdukcjaKanclerz>();
    }
    ~Fabryka()= default;
    shared_ptr<Pionek> stworz(std::string rodzaj,int szybkosc){
        return (*mapa_produkcji[rodzaj])(szybkosc);
    }
private:
    std::map<std::string,shared_ptr<Produkcja>> mapa_produkcji;
};
