//
// Created by Justyna Kryś on 25/12/2019.
//

#ifndef ZAL_CPP_PLANSZA_HH
#define ZAL_CPP_PLANSZA_HH

#include <vector>
#include <iostream>

#include "Pole.h"


using namespace std;


class Plansza{
public:
    Plansza(){};
    Plansza(const string& filename);
    Plansza(int width,int height);
    ~Plansza()=default;
    void wczytaj_plansze(fstream f);
    void drukuj();
    bool ustaw_pionek(int x, int y,shared_ptr<Pionek>);
   // bool przesun_pionek(int x, int y,shared_ptr<Pionek>);
    void zdejmij_pionek(int x,int y);
    void usun_pionek(int x,int y);
    void wczytaj_ustawienie(const string& filename);
    std::vector<Pole>& operator[](int a){return aktualna[a];}
private:
    std::vector<std::vector<Pole> > aktualna;

};
#endif //ZAL_CPP_GRACZ_H