//
// Created by Justyna Kryś on 25/12/2019.
//
#include <unordered_map>
#include <iostream>
#include <fstream>
using namespace std;

#define RESET   "\033[0m"
#define BLACK   "\033[30m"      /* Black */
#define RED     "\033[31m"      /* Red */
#define GREEN   "\033[32m"      /* Green */
#define YELLOW  "\033[33m"      /* Yellow */
#define BLUE    "\033[34m"      /* Blue */
#define MAGENTA "\033[35m"      /* Magenta */
#define CYAN    "\033[36m"      /* Cyan */
#define WHITE   "\033[37m"      /* White */
#define BOLDBLACK   "\033[1m\033[30m"      /* Bold Black */
#define BOLDRED     "\033[1m\033[31m"      /* Bold Red */
#define BOLDGREEN   "\033[1m\033[32m"      /* Bold Green */
#define BOLDYELLOW  "\033[1m\033[33m"      /* Bold Yellow */
#define BOLDBLUE    "\033[1m\033[34m"      /* Bold Blue */
#define BOLDMAGENTA "\033[1m\033[35m"      /* Bold Magenta */
#define BOLDCYAN    "\033[1m\033[36m"      /* Bold Cyan */
#define BOLDWHITE   "\033[1m\033[37m"      /* Bold White */

unordered_map<string, int> read_params(string file_name){
    ifstream fin(file_name); if (!fin.is_open()) {
        cout << "Cannot open file " << file_name << endl;
        cout << "Terminate execution" << endl; }
    unordered_map<string, int> m;
    string par_name;
    int par_value;
    while ( fin >> par_name >> par_value )
        m[par_name] = par_value;
    return m; }

void print_params(unordered_map<string, int> m){ for(auto par: m)
        cout << par.first << ": " << par.second <<GREEN<< endl; }

//int main(int argc, char** argv) {
//    unordered_map<string, int> dict = read_params("pars.txt"); print_params(dict);
//    cout << dict["par1"] << RESET <<endl;
//    cout << dict["I am not here"] << endl; // Returns 0
//    cout << dict.at("par1")<<RED << endl;
//    cout << dict.at("I am absent too") << endl; // Throws exception out_of_range
//}
