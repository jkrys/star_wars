//
// Created by Justyna Kryś on 24/12/2019.
//

#include "Gra.h"
#include "Gracz.cpp"


#include <iostream>
#include <fstream>
#include <sstream>

void Gra::graj(){
    while (czas_gry>0){

        czlowiek->rusz_sie();
        komputer->rusz_sie();
        plansza->drukuj();
        if (czy_koniec())
            kto_wygral();
        czas_gry-=1;
    }
    cout<<"koniec czasu"<<endl;
}


shared_ptr<Plansza> Gra::wez_plansze() {
    return plansza;
}

void Gra::wczytaj(string nazwa_pliku) {
    string linia;
    fstream plik;
    plik.open(nazwa_pliku);

    if (plik.good()) {
        std::vector<Pole> wiersz;

        while (!plik.eof()) {
            getline(plik, linia);
            if (linia == "plansza") {
                plansza = make_shared<Plansza>(nazwa_pliku);
            }

            if (linia == "komputer") {
                cout << "komputer";
                std::stringstream pl;
                getline(plik, linia);
                while (linia[0] != '#') {
                    pl<<linia;
                    pl<<'\n';
                    getline(plik, linia);
                }
                komputer->stworz(pl,fabryka,plansza);
            }
            if (linia == "czlowiek") {
                cout << "czlowiek";
                std::stringstream pl;
                getline(plik, linia);
                while (linia[0] != '#') {
                   // cout<<linia;

                    pl<<linia;
                    pl<<'\n';
                    getline(plik, linia);
                }
                czlowiek->stworz(pl,fabryka,plansza);
            }
            if (linia == "gra") {
                cout << "gra\n";
                string pl;
                getline(plik, linia);
                while (linia[0] != '#') {
                    pl.append(linia);
                    pl.push_back('\n');
                    getline(plik, linia);
                }
                czas_gry = atoi(&pl[0]);
            }
        }

    }
    plik.close();
}
